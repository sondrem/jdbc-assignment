package no.noroff.accelerate.jdbc_assignment.runners;

import no.noroff.accelerate.jdbc_assignment.models.Customer;
import no.noroff.accelerate.jdbc_assignment.repositories.customer.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class AppRunner implements ApplicationRunner {

    private final CustomerRepository customerRepository;

    @Autowired
    public AppRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        /*customerRepository.findAll().forEach(System.out::println);
        System.out.println(customerRepository.findById(33));
        System.out.println(customerRepository.findByName("BeRT Rown"));
        customerRepository.findPageOfCustomers(5, 70).forEach(System.out::println);
        Customer newCustomer = new Customer(92, "Ola", "Nordmann", "Norway", "0854", "54353", "yahoo@fdg.gf");
        customerRepository.insert(newCustomer);
        System.out.println(customerRepository.findById(92));
        //Customer anotherCustomer = new Customer(92, "Kari", "Nordmann", "Norway", "0854", "54353", "yahoo@fdg.gf");
        //System.out.println(customerRepository.update(anotherCustomer));
        System.out.println(customerRepository.findById(92));
        System.out.println(customerRepository.findCountryMaxCustomers());
        System.out.println(customerRepository.findHighestSpender());
        customerRepository.findGenreForCustomer(customerRepository.findById(33)).forEach(System.out::println);*/
    }
}
