package no.noroff.accelerate.jdbc_assignment.repositories.customer;

import no.noroff.accelerate.jdbc_assignment.models.Customer;
import no.noroff.accelerate.jdbc_assignment.models.CustomerCountry;
import no.noroff.accelerate.jdbc_assignment.models.CustomerGenre;
import no.noroff.accelerate.jdbc_assignment.models.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final String url;
    private final String username;
    private final String password;

    public CustomerRepositoryImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     * @return List with all customers in database
     */
    @Override
    public List<Customer> findAll() {
        List<Customer> customers = new ArrayList<>();
        String sql = "SELECT * FROM customer";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle Result
            while (result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }


    /**
     * @param id for a specific customer
     * @return A specific customer from the database
     */
    @Override
    public Customer findById(Integer id) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE customer_id = (?)";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle Result
            while (result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    /**
     * @param name for a specific customer
     * @return A specific customer from the database
     */
    @Override
    public Customer findByName(String name) {
        int index = name.indexOf(' ');
        String firstName = name.substring(0, index);
        String lastName = name.substring(index + 1);
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE first_name ILIKE (?) OR " +
                "last_name ILIKE (?)";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, "%" + firstName + "%");
            statement.setString(2, "%" + lastName + "%");
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle Result
            while (result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    /**
     * @param limit number of entries to be returned
     * @param offset entry point of table
     * @return A page of customers from the database
     */
    @Override
    public List<Customer> findPageOfCustomers(int limit, int offset) {
        List<Customer> customers = new ArrayList<>();
        String sql = "SELECT * FROM customer OFFSET (?) LIMIT (?)";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, offset);
            statement.setInt(2, limit);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle Result
            while (result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    /**
     * @return Country with the most customers
     */
    @Override
    public CustomerCountry findCountryMaxCustomers() {
        CustomerCountry country = null;
        String sql = "SELECT country, count(*) AS number FROM customer " +
                "GROUP BY country ORDER BY number DESC LIMIT 1";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle Result
            while (result.next()) {
                country = new CustomerCountry(
                        result.getString("country"),
                        result.getInt("number")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return country;
    }

    /**
     * This method returns the customer who is the highest spender
     * (total in invoice table is the largest).
     * @return Customer who is the highest spender
     */
    @Override
    public CustomerSpender findHighestSpender() {
        CustomerSpender spender = null;
        String sql = "SELECT customer_id, total FROM invoice WHERE total = " +
                "(SELECT MAX (total) FROM invoice)";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle Result
            while (result.next()) {
                spender = new CustomerSpender(
                        result.getInt("customer_id"),
                        result.getDouble("total")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return spender;
    }

    /**
     * This method finds the most popular genre for a given customer.
     * Most popular in this context
     * means the genre that corresponds to the most tracks from invoices
     * associated to that customer
     * @param customer which customer to find
     * @return The most popular genre for a given customer
     */
    @Override
    public List<CustomerGenre> findGenreForCustomer(Customer customer) {
        List<CustomerGenre> genres = new ArrayList<>();
        String sql = """
                SELECT g.name, count(*) AS most_popular
                FROM invoice AS i
                \tINNER JOIN invoice_line AS il USING(invoice_id)
                \tINNER JOIN track AS t USING(track_id)
                \tINNER JOIN genre AS g USING(genre_id)
                WHERE i.customer_id = (?)
                GROUP BY g.name
                ORDER BY count(*) DESC
                FETCH FIRST 1 ROWS WITH TIES;""";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer.id());
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle Result
            while (result.next()) {
                CustomerGenre cg = new CustomerGenre(
                        result.getString("name"),
                        result.getInt("most_popular")
                );
                genres.add(cg);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return genres;
    }

    /**
     * @param customer to be inserted
     * @return Adds a new customer to the database
     */
    @Override
    public int insert(Customer customer) {
        String sql = "INSERT INTO customer (customer_id, first_name, last_name, " +
                "country, postal_code, phone, email) VALUES (?,?,?,?,?,?,?)";
        int result = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer.id());
            statement.setString(2, customer.firstName());
            statement.setString(3, customer.lastName());
            statement.setString(4, customer.country());
            statement.setString(5, customer.postalCode());
            statement.setString(6, customer.phoneNumber());
            statement.setString(7, customer.email());
            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param customer to be updated
     * @return Updates data for an existing customer
     */
    @Override
    public int update(Customer customer) {

        String sql = "UPDATE customer SET (first_name, last_name, " +
                "country, postal_code, phone, email) = (?,?,?,?,?,?) " +
                "WHERE customer_id = (?)";
        int result = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1, customer.id());
            // Execute statement
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
