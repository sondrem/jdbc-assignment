package no.noroff.accelerate.jdbc_assignment.repositories.customer;

import no.noroff.accelerate.jdbc_assignment.models.Customer;
import no.noroff.accelerate.jdbc_assignment.models.CustomerCountry;
import no.noroff.accelerate.jdbc_assignment.models.CustomerGenre;
import no.noroff.accelerate.jdbc_assignment.models.CustomerSpender;
import no.noroff.accelerate.jdbc_assignment.repositories.CRUDRepository;

import java.util.List;

public interface CustomerRepository extends CRUDRepository<Customer, Integer> {

    Customer findByName(String name);

    List<Customer> findPageOfCustomers(int limit, int offset);

    CustomerCountry findCountryMaxCustomers();

    CustomerSpender findHighestSpender();

    List<CustomerGenre> findGenreForCustomer(Customer customer);
}
