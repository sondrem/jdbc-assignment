package no.noroff.accelerate.jdbc_assignment.models;

public record CustomerCountry(String country, int numberOfCustomers) {
}
