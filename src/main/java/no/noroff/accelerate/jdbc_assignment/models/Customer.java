package no.noroff.accelerate.jdbc_assignment.models;

public record Customer(int id, String firstName, String lastName, String country,
                       String postalCode, String phoneNumber, String email) {
}
