package no.noroff.accelerate.jdbc_assignment.models;

public record CustomerSpender(int id, double amount) {
}
