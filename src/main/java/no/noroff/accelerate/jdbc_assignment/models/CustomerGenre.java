package no.noroff.accelerate.jdbc_assignment.models;

public record CustomerGenre(String genre, int numberOfTracks) {
}
