CREATE DATABASE SuperheroesDb;

/* Ensures we dont try re-create the table */
DROP TABLE IF EXISTS superhero;
CREATE TABLE superhero (
    superhero_id serial PRIMARY KEY,
    superhero_name varchar(30) NOT NULL,
    alias varchar(30),
    origin varchar(30)
);

/* Ensures we dont try re-create the table */
DROP TABLE IF EXISTS assistant;
CREATE TABLE assistant (
    assistant_id serial PRIMARY KEY,
    assistant_name varchar(30) NOT NULL
);

/* Ensures we dont try re-create the table */
DROP TABLE IF EXISTS power;
CREATE TABLE power (
    power_id serial PRIMARY KEY,
    power_name varchar(30) NOT NULL,
    power_description varchar(100) NOT NULL
);



