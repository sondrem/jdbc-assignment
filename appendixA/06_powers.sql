INSERT INTO power (power_name, power_description)
    VALUES ('Fly', 'Makes the superhero fly');
INSERT INTO power (power_name, power_description)
    VALUES ('Teletransportion', 'Makes the superhero able to teletransport');
INSERT INTO power (power_name, power_description)
    VALUES ('Nightvision', 'Makes the superhero see in the dark');
INSERT INTO power (power_name, power_description)
    VALUES ('Mindreader', 'Makes the superhero read every mind');

INSERT INTO superhero_power VALUES (1,3);
INSERT INTO superhero_power VALUES (1,4);
INSERT INTO superhero_power VALUES (2,1);
INSERT INTO superhero_power VALUES (2,3);
INSERT INTO superhero_power VALUES (3,1);
INSERT INTO superhero_power VALUES (3,3);



