# JDBC Assignment

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![pipeline status](https://gitlab.com/NicholasLennox/gradle-ci/badges/master/pipeline.svg)](https://gitlab.com/sondrem/jdbc-assignment/-/commits/main)

Data access with JDBC - SQL scripts and a Spring Boot application in Java

## Table of Contents

- [About](#about)
- [Install](#install)
- [Usage](#usage)
- [Integration](#Integration)
- [Built with](#built-with)
- [Contributing](#contributing)
- [License](#license)

## About
This project is to practice the use of plain Java to create a Spring Boot application 
through Spring Initializr, and SQL scripts to create a database with some given requirements.

### Appendix A: SQL scripts to create database
Several scripts which can be run to create a database, setup some tables 
in the database, add relationships to 
the tables, and then populate the tables with data.

### Appendix B: Reading data with JDBC
Spring Boot application using JDBC with PostgreSQL driver. 
We are given a database to work with called Chinook.
Chinook models the iTunes database of customers purchasing songs. The application
uses a repository pattern to interact with the database.

## Install
Clone repository and build project.

## Usage
Run JdbcAssignmentApplication.java.
At the moment the main method in AppRunner is commented out. 
Remove the commenting to let the application show some examples of
methods. 

## Integration
Includes a gitlab-ci.yml file with a single build stage 
triggered when committing to the main branch.

## Built with

- Java JDK 17
- Spring Initializr
- SQL
- PostGres
- PGAdmin
- Postgres SQL driver dependency
- Gradle - Dependency Management

## Contributing
- [Lars-Inge Gammelsæter Jonsen](https://gitlab.com/Kaladinge)
- [Sondre Mæhre](https://gitlab.com/sondrem)

PRs accepted.

## License

UNLICENSED